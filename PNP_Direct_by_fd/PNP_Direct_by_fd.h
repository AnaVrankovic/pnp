#ifndef PNP_Direct_by_fd_H
#define PNP_Direct_by_fd_H

class PNP_Direct_by_fd 
{

public:
    PNP_Direct_by_fd(const char*);
    virtual ~PNP_Direct_by_fd();
     const char *directory;
    
    void output(const char*);
   
};

#endif 
