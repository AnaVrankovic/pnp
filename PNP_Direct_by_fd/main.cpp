
#include "PNP_Direct_by_fd.h"
#include <iostream>
#include <stdio.h>

int main(int argc, char** argv) {
   if (argc < 3) { 
        std::cout << "Molim upisite ime direktorija i ime zeljene datoteke (npr. \home\\user\\test file1)\n"; 
        std::cin.get();
        exit(0);
    } else {
   const char *directory=argv[1];
    PNP_Direct_by_fd foo(directory);
    foo.output(argv[2]);
    }
}
