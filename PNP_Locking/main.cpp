
#include "PNP_Locking.h"
#include <thread>
#include <stdio.h>
#include <iostream>

int main(int argc, char** argv)
{
      if (argc < 2) { 
        std::cout << "Molim upisite ime zeljene datoteke (npr. file1)\n"; 
        std::cin.get();
        exit(0);
    } else {
    
    PNP_Locking foo(argv[1]);
 
     std::thread t1(&PNP_Locking::exclusiveLock,PNP_Locking(argv[1]));
      std::thread t2(&PNP_Locking::exclusiveLock,PNP_Locking(argv[1]));   
       
      std::thread t3(&PNP_Locking::byteRangeLock,PNP_Locking(argv[1]));
      std::thread t4(&PNP_Locking::byteRangeLock,PNP_Locking(argv[1]));
    std::thread t5(&PNP_Locking::byteRangeLock,PNP_Locking(argv[1]));
     t1.join();
      t2.join();
      t5.join();
      t3.join();
      t4.join();
      
    }
  
}