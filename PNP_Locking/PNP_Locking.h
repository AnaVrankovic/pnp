#ifndef PNP_Locking_H
#define PNP_Locking_H



class PNP_Locking 
{

public:
    PNP_Locking(const char *);
    virtual ~PNP_Locking();
    const char *fileName;
    void exclusiveLock();
    void byteRangeLock();

};

#endif // PNP_Locking_H
