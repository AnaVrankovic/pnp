
#include "PNP_Locking.h"
#include <thread>
#include <stdio.h>
#include <iostream>
#include <process.h>
#include <Windows.h>

   
    
unsigned int __stdcall threadExclusiveLock(void* data) 
{
	 DWORD  dwBytesRead, dwBytesWritten, dwPos;
     HANDLE hFile = ::CreateFileA("lock.txt", GENERIC_READ,FILE_SHARE_READ, 0, OPEN_ALWAYS, 0, 0);
    OVERLAPPED overlapped;
    memset(&overlapped, 0, sizeof(overlapped));
    printf("Uzimam lock\n");
   if (! LockFile(hFile, dwPos, 0, 0, 0))
    {
       
        printf("Greska, file vec zakljucan\n");
    }
    else
    {
        printf("Dobiven lock\n");
        
         UnlockFile(hFile, dwPos, 0, dwBytesRead, 0);
        printf("Pusten lock\n");
	}return 0;
}

unsigned int __stdcall threadByteLock(void* data) 
{
	 HANDLE hFile = ::CreateFileA("lockBlock.txt", GENERIC_READ, FILE_SHARE_READ, 0, OPEN_ALWAYS, 0, 0);
    OVERLAPPED overlapped;
    memset(&overlapped, 0, sizeof(overlapped));
    const int lockSize = 10000;
    printf("Uzimam lock\n");
    if (!LockFileEx(hFile, LOCKFILE_EXCLUSIVE_LOCK, 0, lockSize, 10, &overlapped))
    {
        DWORD err = GetLastError();
        printf("Greska %i\n", err);
    }
    else
    {
        printf("Dobiven lock\n");
        UnlockFileEx(hFile, 0, lockSize,10, &overlapped);
        printf("Pusten lock\n");
	}return 0;
}

int main(int argc, char** argv)
{
      if (argc < 2) { 
        std::cout << "Molim upisite ime zeljene datoteke (npr. file1)\n"; 
        std::cin.get();
        exit(0);
    } else {
    
    

   
    
	HANDLE myhandle[4];

	myhandle[0] = (HANDLE)_beginthreadex(0, 0, &threadExclusiveLock, 0, 0, 0); 
	
	myhandle[1] = (HANDLE)_beginthreadex(0, 0, &threadExclusiveLock, 0, 0, 0);
	WaitForSingleObject(myhandle[0], INFINITE);
	WaitForSingleObject(myhandle[1], INFINITE);
	CloseHandle(myhandle[0]);
	CloseHandle(myhandle[1]);
	system("PAUSE");
	myhandle[3] = (HANDLE)_beginthreadex(0, 0, &threadByteLock, 0, 0, 0); 
	
	myhandle[4] = (HANDLE)_beginthreadex(0, 0, &threadByteLock, 0, 0, 0);
	WaitForSingleObject(myhandle[3], INFINITE);
	WaitForSingleObject(myhandle[4], INFINITE);
	CloseHandle(myhandle[3]);
	CloseHandle(myhandle[4]);
  
    }
}
