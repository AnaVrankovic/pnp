#include "PNP_Locking.h"

#include <fcntl.h>
#include<fstream>
#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <thread>
 
const char *fileName;

PNP_Locking::PNP_Locking(const char *fileName)
{
   this->fileName=fileName;
}

PNP_Locking::~PNP_Locking()
{}

  
 void PNP_Locking::exclusiveLock()
{    
    
    while(-1==open("lockfile", O_EXCL|O_CREAT)) std::cout<<"..."<<std::endl;
    std::ofstream outfile(this->fileName, std::ofstream::out | std::ofstream::app);
    outfile << "Hello world" << std::endl;
    std::cout << "Hello World!" << std::endl;
     outfile.close();
    remove("lockfile");
    
    
  
}

void PNP_Locking::byteRangeLock()
{   
 
  int fd;
    struct flock fl;
  
    fd = open(this->fileName,  O_WRONLY | O_RSYNC);
    if (fd == -1){
       std::cout << "greska otvaranja filea!" << std::endl;
       return;}
    fl.l_type = F_WRLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = 0;
    fl.l_len = 0;
    
    if (fcntl(fd, F_SETLK, &fl) == -1) {
        if (errno == EACCES || errno == EAGAIN) {
           std::cout <<"Zakljucano od strane drugog procesa"<< std::endl;
        } else {
	  std::cout <<"Greska"<< std::endl;
           return;
        }
    } else {  
      
     
      
      std::ofstream outfile(this->fileName, std::ofstream::out | std::ofstream::app);
    outfile << "Hello world!" << std::endl;
    std::cout << "Hello World!" << std::endl;
    


        fl.l_type = F_UNLCK;
        fl.l_whence = SEEK_SET;
        fl.l_start = 0;
        fl.l_len = 0; outfile.close();
        if (fcntl(fd, F_SETLK, &fl) == -1)
           return;}
        
   
           
           
           
           
           return;}
   
