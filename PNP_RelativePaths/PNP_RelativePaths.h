#ifndef PNP_RelativePaths_H
#define PNP_RelativePaths_H



class PNP_RelativePaths 
{

public:
    PNP_RelativePaths(const char*);
    virtual ~PNP_RelativePaths();
     const char *directory;
    
    void output(const char*,const char *);
    void outputAbs(const char*,const char *);
};

#endif // PNP_RelativePaths_H
