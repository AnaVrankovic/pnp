
#include "PNP_RelativePaths.h"

#include <stdio.h>
#include <iostream>
using namespace std;
int main(int argc, char* argv[])
{   
    if (argc < 3) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
        std::cout << "Molim upisite ime direktorija i ime zeljene datoteke (npr. \home\\user\\test file1)\n"; // Inform the user of how to use the program
        std::cin.get();
        exit(0);
    } else {
    const char *directory=argv[1];
    PNP_RelativePaths foo(directory);
    foo.output(argv[1],argv[2]);
    foo.outputAbs(argv[1],argv[2]);
    }
}
