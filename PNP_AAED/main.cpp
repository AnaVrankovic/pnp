#include <thread>
#include <atomic>
#include <string>
#include <stdio.h>
#include <iostream>
#include <assert.h>

std::atomic<std::string*> ptr;
int data;
 
void producer()
{
    std::string* p  = new std::string("Hello");
    data = 42;
    ptr.store(p, std::memory_order_release);
}
 
void consumer()
{
    std::string* p2;
    while (!(p2 = ptr.load(std::memory_order_consume)))
        ;
    assert(*p2 == "Hello");    
    assert(data == 42); 
    std::cout << "Potvrdeno da se u pointeru nalazi string hello i int 42\n";
}

 

int main()
{
      
    std::thread t1(producer);
    std::thread t2(consumer);
    t1.join(); t2.join();
 
}



