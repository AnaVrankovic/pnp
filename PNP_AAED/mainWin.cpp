#include <atomic> 
#include <assert.h> 
#include <string> 
#include <thread> 
 #include <stdio.h>
#include <iostream>
#include <process.h>
#include <Windows.h>
using std::atomic; 
using std::memory_order_consume; 
using std::memory_order_relaxed; 
using std::memory_order_release; 
using std::string; 

 
struct X 
{ 
    int i; 
    string s; 
}; 
 
atomic<X*> p; 
atomic<int> a; 
 
unsigned int __stdcall create_x(void* data) 
{ 
    X* x = new X; 
    x->i = 42; 
    x->s = "hello"; 
    a.store(99, memory_order_relaxed); 
    p.store(x, memory_order_release); 
	return 0;
} 
 
unsigned int __stdcall use_x(void* data) 
{ 
    X* x; 
    while (!(x = p.load(memory_order_consume))) 
    ;
    assert(x->i == 42); 
    assert(x->s == "hello"); 
    assert(a.load(memory_order_relaxed) == 99); 
	  std::cout << "Potvrdeno da se u pointeru nalazi string hello i int 42\n";
	 
	  return 0;
} 
 
int main() 
{ 
    HANDLE myhandle[2];

	myhandle[0] = (HANDLE)_beginthreadex(0, 0, &create_x, 0, 0, 0); 
	
	myhandle[1] = (HANDLE)_beginthreadex(0, 0, &use_x, 0, 0, 0);
	WaitForSingleObject(myhandle[0], INFINITE);
	WaitForSingleObject(myhandle[1], INFINITE);
	CloseHandle(myhandle[0]);
	CloseHandle(myhandle[1]); 

} 