#ifndef PNP_AtomicRenaming_H
#define PNP_AtomicRenaming_H



class PNP_AtomicRenaming 
{

public:
    PNP_AtomicRenaming(const char *);
    virtual ~PNP_AtomicRenaming();
    const char *fileName;
    void output();
};

#endif // PNP_AtomicRenaming_H
