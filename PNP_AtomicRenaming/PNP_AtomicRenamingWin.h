#ifndef PNP_AtomicRenamingWin_H
#define PNP_AtomicRenamingWin_H



class PNP_AtomicRenamingWin
{

public:
    PNP_AtomicRenamingWin(const char *);
    virtual ~PNP_AtomicRenamingWin();
    const char *fileName;
    void output();
};

#endif // PNP_AtomicRenamingWin_H
